#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.PackageManager;
using UnityEditor.PackageManager.Requests;
using System.Linq;
using System;
using System.Collections.Generic;
using UnityEngine;

[InitializeOnLoad]
public class PackageDependenciesHandler
{
    const string PackagesCheckedKey = "PackagesChecked.InSession";
    static ListRequest _listRequest;
    static readonly List<AddRequest> _addRequests = new List<AddRequest>();

    static PackageDependenciesHandler()
    {
        // Temporary removal of the automatic packages instalment due to updates on the QUDO SDK
        /*
        // Start the list request
        _listRequest = Client.List();
        EditorApplication.update += CheckPackages;
        */
    }

    static void CheckPackages()
    {
        try
        {
            // Check if the packages have already been checked in this editor session
            if (SessionState.GetBool(PackagesCheckedKey, false))
            {
                EditorApplication.update -= CheckPackages;
                return;
            }

            // Wait until the list request is complete
            if (_listRequest.IsCompleted)
            {
                if (_listRequest.Status == StatusCode.Success)
                {
                    // Check if the packages are already installed
                    bool isnftSDKInstalled = _listRequest.Result.Any(pkg => pkg.name == "com.blockbastards.qudonftsdk");
                    bool isUniTasksInstalled = _listRequest.Result.Any(pkg => pkg.name == "com.cysharp.unitask");

                    // Add the packages if they are not installed
                    if (!isUniTasksInstalled)
                    {
                        //First install the Uni Task, a dependency of the NFT SDK
                        if (PromptInstall("UniTask") )
                        {
                            _addRequests.Add(Client.Add("https://github.com/Cysharp/UniTask.git?path=src/UniTask/Assets/Plugins/UniTask#2.5.5"));
                            WaitAndRemoveCheck();
                            return;
                        }
                    }

                    if (!isnftSDKInstalled)
                    {
                        if (PromptInstall("NFT SDK"))
                        {
                            _addRequests.Add(Client.Add("https://bitbucket.org/blockbastards/com.blockbastards.qudonftsdk.git"));
                            WaitAndRemoveCheck();
                            return;
                        }
                    }

                    // If the user has already all the packages installed or has denied the installation of the packages,
                    // set to avoid asking them again in the current Unity Editor session.
                    SessionState.SetBool(PackagesCheckedKey, true);
                }
                else if (_listRequest.Status >= StatusCode.Failure)
                {
                    Debug.LogError("Failed to list packages: " + _listRequest.Error.message);
                }

                // Unregister the callback
                EditorApplication.update -= CheckPackages;
            }
        }
        catch (Exception e)
        {
            Debug.LogError($"Error while trying to install QUDO SDK Package dependencies \n Error Message: {e.Message}");
            EditorApplication.update -= CheckPackages; // Ensure to unregister the callback on error
        }
    }
    
    
    // Wait and remove the current Check Packages subscription from the Editor loop
    static void WaitAndRemoveCheck()
    {
        EditorApplication.update -= CheckPackages;
        EditorApplication.update += WaitForInstallation;
    }
    
    // Waits for the first add Request to be completed and the check the packages again 
    static void WaitForInstallation()
    {
        if (_addRequests.Count <= 0)
        {
            EditorApplication.update -= WaitForInstallation;
            return;
        }
        
        // Get the first request made
        AddRequest firstRequest = _addRequests.First();
        
        if (firstRequest.IsCompleted)
        {
            if (firstRequest.Status == StatusCode.Success)
            {
                // Re-run the package check after successful installation
                _listRequest = Client.List();
                EditorApplication.update += CheckPackages;
            }
            else if (firstRequest.Status >= StatusCode.Failure)
            {
                Debug.LogError("Failed to install package: " + firstRequest.Error.message);
            }

            // Unregister the callback and remove the cached request from the list
            EditorApplication.update -= WaitForInstallation;
            _addRequests.Remove(firstRequest);
        }
    }

    static bool PromptInstall(string packageName)
    {
        return EditorUtility.DisplayDialog(
            $"Install {packageName}",
            $"The package {packageName} is not installed. Would you like to install it?\nThis is a necessary package for the QUDO SDK to work correctly.",
            "Yes",
            "No"
        );
    }
}
#endif