﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace QUDOSDK.Scripts
{
    public class QUDOPasswordInput : MonoBehaviour
    {
        [SerializeField] private Sprite showPasswordSprite;
        [SerializeField] private Sprite hidePasswordSprite;
        [SerializeField] private Button button;
        [SerializeField] private TMP_Text passwordValidationText;

        private bool showingPassword;
        private bool buttonPressed;
        private TMP_InputField passwordInputField;

        private void Awake()
        {
            passwordInputField = GetComponent<TMP_InputField>();
        }

        private void OnEnable()
        {
            button.onClick.AddListener(TogglePasswordView);
            passwordInputField.onDeselect.AddListener(OnPasswordInputDeselect);
        }

        private void OnDisable()
        {
            button.onClick.RemoveListener(TogglePasswordView);
            passwordInputField.onDeselect.RemoveListener(OnPasswordInputDeselect);
        }

        private void OnPasswordInputDeselect(string text)
        {
            StartCoroutine(CheckDeselectionDelayed());
        }

        private IEnumerator CheckDeselectionDelayed()
        {
            yield return new WaitForSeconds(.1f);
            if (showingPassword && !buttonPressed)
                HidePassword();
        }

        private void TogglePasswordView()
        {
            buttonPressed = true;
            StartCoroutine(HoldButtonStatus());
            showingPassword = !showingPassword;
            ((Image) button.targetGraphic).sprite = showingPassword ? hidePasswordSprite : showPasswordSprite;
            passwordInputField.contentType =
                showingPassword ? TMP_InputField.ContentType.Standard : TMP_InputField.ContentType.Password;
            passwordInputField.ForceLabelUpdate();
            passwordInputField.Select();
        }

        private IEnumerator HoldButtonStatus()
        {
            yield return new WaitForSeconds(.2f);
            buttonPressed = false;
        }

        private void HidePassword()
        {
            showingPassword = false;
            ((Image) button.targetGraphic).sprite = showPasswordSprite;
            passwordInputField.contentType = TMP_InputField.ContentType.Password;
            passwordInputField.ForceLabelUpdate();
        }
    }
}