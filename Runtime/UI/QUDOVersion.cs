using QUDOSDK;
using TMPro;
using UnityEngine;

public class QUDOVersion : MonoBehaviour
{

    [SerializeField] private TMP_Text VersionText;
    
    // Start is called before the first frame update
    void Start()
    {
        if (VersionText == null)
        {
            return;
        }
        
        VersionText.text = QUDO.SDK_VERSION;
    }
}
