﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace QUDOSDKScripts
{
    //[RequireComponent(typeof(Canvas))]
    public class CanvasHelperQUDO : MonoBehaviour
    {
        public static UnityEvent onOrientationChange = new UnityEvent();
        public static UnityEvent onResolutionChange = new UnityEvent();
        public static bool isLandscape { get; private set; }


        public RectTransform TargetObject;

        [Header("Settings")]

        [SerializeField]
        private Canvas _baseCanvas;

        [SerializeField]
        private bool _ignoreRight;
        [SerializeField]
        private bool _ignoreLeft;
        [SerializeField]
        private bool _ignoreUp;
        [SerializeField]
        private bool _ignoreDown;

        private static List<CanvasHelperQUDO> helpers = new List<CanvasHelperQUDO>();

        private static bool screenChangeVarsInitialized = false;
        private static ScreenOrientation lastOrientation = ScreenOrientation.Portrait;
        private static Vector2 lastResolution = Vector2.zero;
        private static Rect lastSafeArea = Rect.zero;

        private RectTransform rectTransform;

        private static Rect _safeArea
        {
            get
            {
                if (false) //debug?
                {
                    return Rect.zero; //Custom safe area here!
                }
                else
                {
                    return Screen.safeArea;
                }
            }
        }

        void Awake()
        {
            if (!helpers.Contains(this))
                helpers.Add(this);

            //canvas = GetComponent<Canvas>();
            rectTransform = GetComponent<RectTransform>();

            //safeAreaTransform = transform.Find("SafeArea") as RectTransform;

            if (!screenChangeVarsInitialized)
            {
                lastOrientation = Screen.orientation;
                lastResolution.x = Screen.width;
                lastResolution.y = Screen.height;
                lastSafeArea = _safeArea;

                screenChangeVarsInitialized = true;
            }
        }

        void Start()
        {
            ApplySafeArea();
        }

        void Update()
        {
            if (helpers[0] != this)
                return;

            if (Application.isMobilePlatform || Debug.isDebugBuild)
            {
                if (Screen.orientation != lastOrientation)
                    OrientationChanged();

                if (_safeArea != lastSafeArea)
                    SafeAreaChanged();
            }
            else
            {
                //resolution of mobile devices should stay the same always, right?
                // so this check should only happen everywhere else
                if (Screen.width != lastResolution.x || Screen.height != lastResolution.y)
                    ResolutionChanged();
            }
        }

        void ApplySafeArea()
        {
            if (TargetObject == null)
                return;

            var safeArea = _safeArea;
            var canvas = _baseCanvas;

            var anchorMin = safeArea.position;
            var anchorMax = safeArea.position + safeArea.size;
            anchorMin.x /= canvas.pixelRect.width;
            anchorMin.y /= canvas.pixelRect.height;
            anchorMax.x /= canvas.pixelRect.width;
            anchorMax.y /= canvas.pixelRect.height;

            if (_ignoreLeft)
                anchorMin.x = 0;
            if (_ignoreRight)
                anchorMax.x = 0;
            if (_ignoreDown)
                anchorMin.y = 0;
            if (_ignoreUp)
                anchorMax.y = 0;

            TargetObject.anchorMin = anchorMin;
            TargetObject.anchorMax = anchorMax;
        }

        void OnDestroy()
        {
            if (helpers != null && helpers.Contains(this))
                helpers.Remove(this);
        }

        private static void OrientationChanged()
        {
            //Debug.Log("Orientation changed from " + lastOrientation + " to " + Screen.orientation + " at " + Time.time);

            lastOrientation = Screen.orientation;
            lastResolution.x = Screen.width;
            lastResolution.y = Screen.height;

            isLandscape = lastOrientation == ScreenOrientation.LandscapeLeft || lastOrientation == ScreenOrientation.LandscapeRight;
            onOrientationChange.Invoke();

        }

        private static void ResolutionChanged()
        {
            if (lastResolution.x == Screen.width && lastResolution.y == Screen.height)
                return;

            lastResolution.x = Screen.width;
            lastResolution.y = Screen.height;

            isLandscape = Screen.width > Screen.height;
            onResolutionChange.Invoke();
        }

        private static void SafeAreaChanged()
        {
            if (lastSafeArea == _safeArea)
                return;

            lastSafeArea = _safeArea;

            for (int i = 0; i < helpers.Count; i++)
            {
                helpers[i].ApplySafeArea();
            }
        }

        public static Vector2 GetCanvasSize()
        {
            return helpers[0].rectTransform.sizeDelta;
        }

        public static Vector2 GetSafeAreaSize()
        {
            for (int i = 0; i < helpers.Count; i++)
            {
                if (helpers[i].TargetObject != null)
                {
                    return helpers[i].TargetObject.sizeDelta;
                }
            }
            return GetCanvasSize();
        }
    }
}