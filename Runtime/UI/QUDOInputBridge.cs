using QUDOSDK.InputHandler;
using UnityEngine;
#if ENABLE_INPUT_SYSTEM
using System.Linq;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.EnhancedTouch;
using Touch = UnityEngine.InputSystem.EnhancedTouch.Touch;
using Gyroscope = UnityEngine.InputSystem.Gyroscope;
using UnityEngine.InputSystem.Controls;
using TouchPhase = UnityEngine.InputSystem.TouchPhase;
using System;
using UnityEngine.EventSystems;
#elif ENABLE_LEGACY_INPUT_MANAGER
using UnityEngine.EventSystems;
using System;
#endif

namespace QUDOSDK.Scripts
{
    public class QUDOInputBridge : QUDOInputHandler
    {
#if ENABLE_LEGACY_INPUT_MANAGER
        private bool _confirmedThisFrame;

        private bool _hasInputModule;
        private string _horizontalAxis = "Horizontal";
        private string _verticalAxis = "Vertical";
#endif

        private void Awake()
        {
#if ENABLE_INPUT_SYSTEM
            // Enable the enhanced touch support
            if (!EnhancedTouchSupport.enabled)
            {
                EnhancedTouchSupport.Enable();
            }

            // Enable the gyroscope
            if (Gyroscope.current != null)
            {
                InputSystem.EnableDevice(Gyroscope.current);
            }

            // Enable the gravity sensor
            if (GravitySensor.current != null)
            {
                InputSystem.EnableDevice(GravitySensor.current);
            }
#endif
        }

#if ENABLE_LEGACY_INPUT_MANAGER
        public override void CheckInputSystem()
        {
            _hasInputModule = EventSystem.current && EventSystem.current.currentInputModule &&
                              EventSystem.current.currentInputModule is StandaloneInputModule;
            if (_hasInputModule)
            {
                var standaloneInputModule = EventSystem.current.currentInputModule as StandaloneInputModule;
                if (standaloneInputModule)
                {
                    _horizontalAxis = standaloneInputModule.horizontalAxis;
                    _verticalAxis = standaloneInputModule.verticalAxis;
                }
                else
                {
                    _hasInputModule = false;
                }
            }
        }
#endif

        public override bool IsLegacyInputSystem()
        {
#if ENABLE_LEGACY_INPUT_MANAGER
            return true;
#elif ENABLE_INPUT_SYSTEM
            return false;
#else
            return false;
#endif
        }

        public override void SetGyroscopeStatus(bool newStatus)
        {
#if ENABLE_LEGACY_INPUT_MANAGER
            Input.gyro.enabled = newStatus;
#elif ENABLE_INPUT_SYSTEM
            if (newStatus)
            {
                InputSystem.EnableDevice(Gyroscope.current);
            }
            else
            {
                InputSystem.DisableDevice(Gyroscope.current);
            }
#endif
        }

        public override bool GetGyroscopeStatus()
        {
#if ENABLE_LEGACY_INPUT_MANAGER
            return Input.gyro.enabled;
#elif ENABLE_INPUT_SYSTEM
            return Gyroscope.current is { enabled: true };
#else
            return false;
#endif
        }

        public override Vector3 GetGyroscopeData()
        {
#if ENABLE_LEGACY_INPUT_MANAGER
            if (Input.gyro.enabled)
                return Input.gyro.userAcceleration;
            else
                return Vector3.zero;
#elif ENABLE_INPUT_SYSTEM
            if (Gyroscope.current is { enabled: true })
            {
                return Gyroscope.current.angularVelocity.ReadValue();
            }
            else
            {
                return Vector3.zero;
            }
#else
            return Vector3.zero;
#endif
        }

        public override void SetGravityStatus(bool newStatus)
        {
#if ENABLE_LEGACY_INPUT_MANAGER
            //Input.acceleration.enabled = newStatus;
#elif ENABLE_INPUT_SYSTEM
            if (newStatus)
            {
                InputSystem.EnableDevice(GravitySensor.current);
            }
            else
            {
                InputSystem.DisableDevice(GravitySensor.current);
            }
#endif
        }

        public override bool GetGravityStatus()
        {
#if ENABLE_LEGACY_INPUT_MANAGER
            //return Input.ac.enabled;
            return false;
#elif ENABLE_INPUT_SYSTEM
            return GravitySensor.current is { enabled: true };
#else
            return false;
#endif
        }

        public override Vector3 GetGravityData()
        {
#if ENABLE_LEGACY_INPUT_MANAGER
            return Input.acceleration;
            /*if (Input.gyro.enabled)
                return Input.gyro.userAcceleration;
            else
                return Vector3.zero;*/
#elif ENABLE_INPUT_SYSTEM
            if (GravitySensor.current is { enabled: true })
            {
                return GravitySensor.current.gravity.ReadValue();
            }
            else
            {
                return Vector3.zero;
            }
#else
            return Vector3.zero;
#endif
        }

        public override Vector2 GetMousePosition()
        {
#if ENABLE_LEGACY_INPUT_MANAGER
            return Input.mousePosition;
#elif ENABLE_INPUT_SYSTEM
            if (Mouse.current != null)
            {
                return Mouse.current.position.ReadValue();
            }
            else
            {
                return Vector2.zero;
            }
#else
            return Vector2.zero;
#endif
        }

        public override bool GetMouseButtonDown(int button)
        {
#if ENABLE_LEGACY_INPUT_MANAGER
            return Input.GetMouseButtonDown(button);
#elif ENABLE_INPUT_SYSTEM
            if (Mouse.current != null)
            {
                switch (button)
                {
                    // Assuming that left button is the primary key '0'
                    case 0:
                        return Mouse.current.leftButton.wasPressedThisFrame;
                    case 1:
                        return Mouse.current.rightButton.wasPressedThisFrame;
                    case 2:
                        return Mouse.current.middleButton.wasPressedThisFrame;
                    default:
                        return false;
                }
            }
            else
            {
                return false;
            }
#else
            return false;
#endif
        }

        public override bool GetMouseButtonUp(int button)
        {
#if ENABLE_LEGACY_INPUT_MANAGER
            return Input.GetMouseButtonUp(button);
#elif ENABLE_INPUT_SYSTEM
            if (Mouse.current != null)
            {
                switch (button)
                {
                    // Assuming that left button is the primary key '0'
                    case 0:
                        return Mouse.current.leftButton.wasReleasedThisFrame;
                    case 1:
                        return Mouse.current.rightButton.wasReleasedThisFrame;
                    case 2:
                        return Mouse.current.middleButton.wasReleasedThisFrame;
                    default:
                        return false;
                }
            }
            else
            {
                return false;
            }
#else
            return false;
#endif
        }

        public override bool GetMouseButton(int button)
        {
#if ENABLE_LEGACY_INPUT_MANAGER
            return Input.GetMouseButton(button);
#elif ENABLE_INPUT_SYSTEM
            if (Mouse.current != null)
            {
                switch (button)
                {
                    // Assuming that left button is the primary key '0'
                    case 0:
                        return Mouse.current.leftButton.isPressed;
                    case 1:
                        return Mouse.current.rightButton.isPressed;
                    case 2:
                        return Mouse.current.middleButton.isPressed;
                    default:
                        return false;
                }
            }
            else
            {
                return false;
            }
#else
            return false;
#endif
        }

        public override bool GetKey(KeyCode keyCode)
        {
#if ENABLE_LEGACY_INPUT_MANAGER
            return Input.GetKey(keyCode);
#elif ENABLE_INPUT_SYSTEM
            if (Keyboard.current != null)
                switch (keyCode)
                {
                    case KeyCode.LeftShift:
                        return Keyboard.current.leftShiftKey.isPressed;
                    case KeyCode.R:
                        return Keyboard.current.rKey.isPressed;
                    default:
                        return false;
                }
            else
            {
                return false;
            }
#else
            return false;
#endif
        }

        // TODO: test the key down for the new input system
        public override bool GetKeyDown(KeyCode keyCode)
        {
#if ENABLE_LEGACY_INPUT_MANAGER
            return Input.GetKeyDown(keyCode);
#elif ENABLE_INPUT_SYSTEM
            if (Keyboard.current != null)
                switch (keyCode)
                {
                    case KeyCode.LeftShift:
                        return Keyboard.current.leftShiftKey.isPressed;
                    case KeyCode.R:
                        return Keyboard.current.rKey.isPressed;
                    default:
                        return false;
                }
            else
            {
                return false;
            }
#else
            return false;
#endif
        }

        public override bool TouchSupported()
        {
#if ENABLE_LEGACY_INPUT_MANAGER
            return Input.touchSupported;
#elif ENABLE_INPUT_SYSTEM
            if (Touchscreen.current != null)
            {
                return Touchscreen.current.enabled;
            }
            else
            {
                return false;
            }
#else
            return false;
#endif
        }

        public override int TouchCount()
        {
#if ENABLE_LEGACY_INPUT_MANAGER
            return Input.touchCount;
#elif ENABLE_INPUT_SYSTEM
            return Touch.activeTouches.Count;
#else
            return 0;
#endif
        }

        public override Vector2 GetTouchPosition(int touchID)
        {
#if ENABLE_LEGACY_INPUT_MANAGER
            return Input.GetTouch(touchID).position;
#elif ENABLE_INPUT_SYSTEM
            if (Touchscreen.current != null)
            {
                return Touch.activeTouches[touchID].screenPosition;
            }
            else
            {
                return Vector2.zero;
            }
#else
            return Vector2.zero;
#endif
        }

        public override QUDOTouchPhase GetTouchPhase(int touchID)
        {
#if ENABLE_LEGACY_INPUT_MANAGER
            switch (Input.GetTouch(touchID).phase)
            {
                case UnityEngine.TouchPhase.Began:
                    return QUDOTouchPhase.BEGAN;
                case UnityEngine.TouchPhase.Canceled:
                    return QUDOTouchPhase.CANCELED;
                case UnityEngine.TouchPhase.Ended:
                    return QUDOTouchPhase.ENDED;
                case UnityEngine.TouchPhase.Moved:
                    return QUDOTouchPhase.MOVED;
                case UnityEngine.TouchPhase.Stationary:
                    return QUDOTouchPhase.STATIONARY;
                default:
                    return QUDOTouchPhase.NONE;
            }
#elif ENABLE_INPUT_SYSTEM
            if (Touch.activeTouches.Count >= touchID)
            {
                return (QUDOTouchPhase)(int)Touch.activeTouches[touchID].phase;
            }
            else
            {
                return QUDOTouchPhase.NONE;
            }
#else
            return QUDOTouchPhase.None;
#endif
        }

        public override bool GetControllerIsPressed(string buttonName)
        {
#if ENABLE_LEGACY_INPUT_MANAGER
            return Input.GetKey(buttonName);
#elif ENABLE_INPUT_SYSTEM
            return Gamepad.current != null && Gamepad.current[buttonName].IsPressed();
#else
            return false;
#endif
        }

        public override int GetControllerCount()
        {
#if ENABLE_LEGACY_INPUT_MANAGER
            return Input.GetJoystickNames().Length;
#elif ENABLE_INPUT_SYSTEM
            return Gamepad.all.Count;
#else
            return 0;
#endif
        }

        public override bool AnyGamepadPressed()
        {
#if ENABLE_LEGACY_INPUT_MANAGER
            // TODO: reformat needed for old input system
            var anyPress = Input.anyKeyDown;
            try
            {
                anyPress |= Math.Abs(Input.GetAxisRaw(_horizontalAxis)) >= .8f;
            }
            catch
            {
                // ignored
            }

            try
            {
                anyPress |= Math.Abs(Input.GetAxisRaw(_verticalAxis)) >= .8f;
            }
            catch
            {
                // ignored
            }

            if (anyPress && !_confirmedThisFrame)
            {
                _confirmedThisFrame = true;
                return true;
            }
            else if (!anyPress)
            {
                _confirmedThisFrame = false;
            }
            else
            {
                return false;
            }

            return false;
#elif ENABLE_INPUT_SYSTEM
            return Gamepad.current != null && Gamepad.current.allControls.Where(x => x is ButtonControl)
                .Any(x => ((ButtonControl)x).wasPressedThisFrame);
#else
            return false;
#endif
        }

        public override bool AnyKeyPressed()
        {
#if ENABLE_LEGACY_INPUT_MANAGER
            return Input.anyKeyDown;
#elif ENABLE_INPUT_SYSTEM
            return Keyboard.current != null && Keyboard.current.allKeys.Any(x => x.wasPressedThisFrame);
#else
            return false;
#endif
            
        }
    }
}