﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AutoLayoutQUDO : MonoBehaviour
{
    public float Spacing;

    public TextAnchor ChildAlignement;

    [Header("Padding")]
    public float Left;
    public float Right;
    public float Top;
    public float Bottom;

    [Header("Child Controls Size")]

    public bool ChildControlHeight;
    public bool ChildControlWidth;

    [Header("Child Force Expand")]

    public bool childForceExpandHeight;
    public bool childForceExpandWidth;

    //Store the screen resolution so that we can compare it later
    private int _screenWidth;

    private void OnEnable()
    {
        _screenWidth = Screen.width;
        UpdateLayout();
    }

    private void Update()
    {
        //If we detect a change on the orientation
        if(_screenWidth != Screen.width)
        {
            UpdateLayout();
            _screenWidth = Screen.width;
        }  
    }

    void UpdateLayout()
    {
        //This means that the screen is in landscape mode
        if (Screen.width >= Screen.height)
            SwitchLayout(true);
        //This means that the screen is in portrait mode
        else
            SwitchLayout(false);
    }

    public void SwitchLayout(bool flipHorizontal)
    {
        //If we already have an horizontal layout group
        if (!flipHorizontal)
        {
            if (!(gameObject.GetComponent<HorizontalLayoutGroup>() is var hLG) || gameObject.GetComponent<VerticalLayoutGroup>())
                return;
            DestroyImmediate(hLG, false);
            var verticalLG = gameObject.AddComponent<VerticalLayoutGroup>();
            verticalLG.spacing = Spacing;
            verticalLG.childControlHeight = ChildControlHeight;
            verticalLG.childControlWidth = ChildControlWidth;
            verticalLG.childForceExpandHeight = childForceExpandHeight;
            verticalLG.childForceExpandWidth = childForceExpandWidth;
            verticalLG.childAlignment = ChildAlignement;
            return;
        }
        else
        {
            if (!(gameObject.GetComponent<VerticalLayoutGroup>() is var vLG) || gameObject.GetComponent<HorizontalLayoutGroup>())
                return;
            DestroyImmediate(vLG, false);
            var horizontalLG = gameObject.AddComponent<HorizontalLayoutGroup>();
            horizontalLG.spacing = Spacing;
            horizontalLG.childControlHeight = ChildControlHeight;
            horizontalLG.childControlWidth = ChildControlWidth;
            horizontalLG.childForceExpandHeight = childForceExpandHeight;
            horizontalLG.childForceExpandWidth = childForceExpandWidth;
            horizontalLG.childAlignment = ChildAlignement;
            return;
        }
    }
}
