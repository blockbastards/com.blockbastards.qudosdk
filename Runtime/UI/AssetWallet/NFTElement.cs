using QUDOSDK;
using QUDOSDK.Data;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace NFTSDK.UI
{
    public class NFTElement : MonoBehaviour
    {
        [Header("UI Elements")] 
        [SerializeField] private TMP_Text nftName;

        [SerializeField] private Image nftProfileImage;

        [SerializeField] private Button nftButton;

        [SerializeField] private Toggle nftToggle;

        private IGAData _nftData;
        
        public void SetupElement(string name, Sprite nftImage, IGAData nftData)
        {
            if (nftName == null || nftProfileImage == null)
            {
                Debug.LogWarning("Can't setup element, dependencies are not defined", gameObject);
                return;
            }

            SetupUIListeners();

            nftName.text = name;
            nftProfileImage.sprite = nftImage;
            _nftData = nftData;
        }

        public void AddButtonAction(UnityAction onButtonClick)
        {
            if (nftButton == null)
            {
                Debug.LogWarning("Can't add behavior to button, dependency not defined", gameObject);
                return;
            }

            nftButton.onClick.AddListener(onButtonClick);
        }

        private void OnToggleClicked(bool onToggle)
        {
            var currUser = QUDO.GetActiveUserSession();

            if (currUser == null)
            {
                Debug.LogWarning("NFTSDK: No active user session, please check this");
                return;
            }
            
            currUser.AssetWallet.PrintImportedAssets();

            if (onToggle)
            {
                currUser.AssetWallet.AddImportedAsset(_nftData);
            }
            else
            {
                currUser.AssetWallet.RemoveImportedAsset(_nftData);
            }
            
            currUser.AssetWallet.PrintImportedAssets();
        }

        private void SetupUIListeners()
        {
            if (nftToggle == null || nftButton == null)
            {
                Debug.LogWarning("UI Elements are not defined, this element will not be initialized correctly!");
                return;
            }

            nftToggle.onValueChanged.AddListener(OnToggleClicked);
        }

        private void OnDestroy()
        {
            nftButton.onClick.RemoveAllListeners();
        }
    }
}
