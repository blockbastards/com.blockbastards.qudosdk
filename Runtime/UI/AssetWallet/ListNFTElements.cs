using System.Threading.Tasks;
using QUDOSDK.Data;
using QUDOSDK;
using UnityEngine;

namespace NFTSDK.UI
{
    public class ListNFTElements : MonoBehaviour
    {
        [Header("Settings")]
        [Tooltip("By default we load the list with Assets, but this can be use to load specific Assets")]
        [SerializeField]
        private bool LoadOnStart = true;

        [Header("UI Elements")] [SerializeField]
        private RectTransform parentContent;

        [SerializeField] private NFTElement prefabNftElement;
        [SerializeField] private GameObject LoadingArea;

        //Internal Variables
        private GameObject _currObject;

        /** -- RESUME --
         * Sample Code for Listing all NFTs and for checking its model and values
         * For this to happen we need to use the NFTPortal:
         * - NFTPortal -> Singleton that has functions relative to retrieving NFTs
         * - To retrieve the NFTs and use then in run-time, you will need a specific Username from the QUDO User class
         * - The NFTs have a NFTData representation, that holds important data that can be used to create the NFT GameObject in game(Id, type, etc)
         * - NOTE: For now the NFTPortal will provide a function to load all the available NFTs. This a temporary function that will be removed in the future
        */

        private void Start()
        {
            if (LoadOnStart)
            {
                LoadingArea.SetActive(true);
                LoadUserIGA();
            }
        }


        /// <summary>
        /// Loads all NFTs asynchronously
        /// Creates UI elements based on the NFTs that come from the NFT API 
        /// </summary>
        private async void LoadUserIGA()
        {
            //Normal code for retrieving the QUDO User NFTs 
            //var nftData = await NFTPortal.LoadUserNFTsAsync(_currUser);
            //Temporary code for retrieving all the NFTs, this will be removed in the future

            var currentUser = QUDO.GetActiveUserSession();
            if (currentUser == null)
            {
                Debug.LogWarning("No user is logged-in/active!");
                return;
            }
        
            var data = await currentUser.AssetWallet.LoadAssets();
            
            if (LoadingArea)
                LoadingArea.SetActive(false);
            
            foreach (IGAData nft in data)
            {
                if (!nft.IsValid()) continue;
                //Load the NFT and create a specific UniTask for each nft, so that it doesn't stop the rest of the code execution
                await SetupListElement(nft);
            }
        }

        /// <summary>
        /// Adds a new element to the list of UI elements, using the nftData as reference
        /// </summary>
        /// <param name="nftData">The nftData that we want to use</param>
        private async Task SetupListElement(IGAData igaData)
        {
            var newNftElement = Instantiate(prefabNftElement, parentContent);

            newNftElement.SetupElement(igaData.Name, await GetNFTImageAsync(igaData), igaData);
        }


        /// <summary>
        /// Gets the associated NFT Image asynchronously using the IMAGECID
        /// </summary>
        /// <param name="nftData">The current nftData</param>
        /// <returns>The Sprite created from the NFT image</returns>
        private async Task<Sprite> GetNFTImageAsync(IGAData igaData)
        {
            if (!igaData.IsValid())
            {
                Debug.LogWarning("NFT data used to Get the NFT Image is not valid");
                return null;
            }

            //Get the NFT image and create a Sprite base on that texture
            var image = await igaData.GetImage();

            return Sprite.Create(image, new Rect(0, 0, image.width, image.height), Vector2.zero);
        }
    }
}