<img src="https://qudo.io/documents/assets/img/logo-portrait.png" alt="" style="width:15%"/>

# QUDO SDK

This repository contains the QUDO SDK package that needs to be imported through the package manager in unity.

## ⚠️ NOTES

If you have an older version of the SDK (non-package) you will have to remove it in order to prevent any issues 
(remove the 'QUDOSDK' and 'QUDOSDKSettings' under the 'Assets' folder), and restart unity.

## Installation

1 - Open unity's package manager (Window -> Package Manager)

2 - Click on the '+' button and select "Add package from git URL..."

3 - Insert the url : http://bitbucket.org/blockbastards/com.blockbastards.qudosdk.git

4 - Wait for the package to import (restart unity if you don't see the "QUDO" button on the menu bar)

5 - Done!


## Documentation

The documentation contains all that you need to start using the QUDO SDK on your game, you should give it a read before starting to implement so that you can have an idea on how it works first. 

The documentation can be found [here](https://qudo.io/sdk-documentation)



## QUDO Website

If you want to know more about the project and keep up with the updates, you can visit us [here](https://qudo.io/)



## QUDO Dashboard

The dashboard is used to register your games, it's where you can manage its achievements, high scores and the resources.

You can access it [here](https://games.qudo.io)



## Support

If you are having trouble and need our help, you can always contact us through the official channels:

![Discord](https://qudo.io/assets/bitbucket/discord-64x64.png) - https://discord.gg/UK9MHRZ

![Telegram](https://qudo.io/assets/bitbucket/telegram-50x50.png)  - https://t.me/qudocoin

Or send us an [email](mailto:qudo.support@blockbastards.io) ✉️
